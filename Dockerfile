# Dockerfile to create python environment
FROM python:3.10

# Installs packages
RUN apt update && apt install -y \
	pip

# Copies the example scripts and install scripts
RUN mkdir -p /app/examples
COPY examples/  app/examples/

# Sets up the environment
WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}
RUN ln -s /bin/python3 /bin/python
RUN export PIP_DEFAULT_TIMEOUT=300

# Installs the packages
RUN pip install visnet1d

# Runs the a script in poetry
CMD ["python", "/app/examples/vessel_network.py"]
