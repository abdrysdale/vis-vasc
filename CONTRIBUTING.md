# Contributing

1. Fork on GitLab.
2. Clone your fork locally locally with:
    ``git clone git@gitlab.com:abdrysdale/visnet1d.git``
3. Follow the installation instructions outlined in the [README](README.md).
4. A list of outstanding feature requests and bugs can be found on our GitLab [issue tracker](https://gitlab.com/abdrysdale/visnet1d/-/issues>).
   - Pick an unassigned issue and comment that you are going to do it.
   - Your GitLab personal label will then be assigned to that issue.
   - Feel free to submit new issues too!
5. Create a new descriptively named branch off the main branch.
	- It may be helpful to use the below table for branch naming where `<desc>` is a concise description of the goal with *ideally* the issue number:
	
	| Issue Label | Suggested Branch Name |
	|:--- | :---|
	| `bug` 	| `fix/<desc>`	|
	| `doc` 	| `doc/<desc>` 	|
	| `enhancement` | `enchance/<desc>`	|
	| `feature`	| `feature/<desc>` 	|

    - Branches are created with:
     ``git checkout -b <branch-name>``
6. Fix the problem and ensure that the fix passes all tests (write more tests if you need).
   	- For bugs, you should write tests such that the tests fails before the fix and passes after the fix.
7. Regularly push your local topic changes back to GitLab:
   ``git push origin <branch-name>``
8. Once you work is ready (or if you'd like some help or feedback) submit a pull request for review.
   	- This is done by switching to the branch name on GitLab and then click *Create merge request*.
    - Add a comment about your branch.
	- After your work has been reviewed and approved it will be merged into the main branch.
9. If anything doesn't work or seems unclear, please contact me!

## Resources:

- For a guide on using git see [here](https://www.freecodecamp.org/news/what-is-git-and-how-to-use-it-c341b049ae61/).
- This process uses the GitHub branch workflow and more information can be found [here](https://guides.github.com/introduction/flow/). 
	- For a comparison of the different branch workflows see [here](https://www.gitkraken.com/learn/git/best-practices/git-branch-strategy).
