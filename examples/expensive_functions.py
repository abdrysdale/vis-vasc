#! /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
import pandas as pd

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
import visnet1d

# Instead of using a function that is evaluated with each update of input parameters, we can use visnet1d
# to explore an output. To do this, we take the output of a very expensive function and pass that into
# an inbuilt visnet1d function.
# As an example, say our expensive function has produced the output: out_mat and t
out_mat = np.sin(np.linspace(0, 10 * np.pi, 100 * 50).reshape(100, 50))
t = np.linspace(0, 10)

# The boundaries must now the indexes of each location.
boundaries = {     # Locations can be selected from a drop-down menu
        "Vessel 1" : [0, 20],          # [Start index, End index]
        "Vessel 2" : [20, 100],  
}

fn = visnet1d.get_static_function(out_mat, t, axis=0)

site = visnet1d.Site(
        fn,                     # Function to plot.
        boundaries,             # Locations to select.
        input_dict_list=None,   # Set to None as the function requires no inputs.
        xlab="Time (s)",        # X-axis label
        ylab="Response",        # Y-axis label
        title="My title",       # Title
)

# Runs the server with the default arguments - same as site.run()
site.run(host="127.0.0.1", port="8050", debug=False)
