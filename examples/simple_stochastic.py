"""An example script showing the use of a custom plot for a stochastic function"""
#! /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
import pandas as pd

# Local imports
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
import visnet1d

# Sets pandas plotting backend
pd.options.plotting.backend = "plotly"

# Defines a simple stochastic function
def foobar(x, **kwargs):     # pylint: disable=invalid-name
    """An example stochastic function"""

    # Gets keyword arguments
    tmax = kwargs.get('tmax', 1)
    a_mean = kwargs.get('a_mean', 1)
    a_stdev = kwargs.get('a_stdev', 1)
    b_mean = kwargs.get('b_mean', 2)
    b_stdev = kwargs.get('b_stdev', 2)
    c_mean = kwargs.get('c_mean', 3)
    c_stdev = kwargs.get('c_stdev', 3)

    t = np.linspace(0, tmax)

    # Gets a, b and c from a normal distribution
    rng = np.random.default_rng()
    a = rng.normal(loc=a_mean, scale=a_stdev)   # pylint: disable=invalid-name
    b = rng.normal(loc=b_mean, scale=b_stdev)   # pylint: disable=invalid-name
    c = rng.normal(loc=c_mean, scale=c_stdev)   # pylint: disable=invalid-name

    y = a * np.sin(2 * np.pi * x / b + 2 * np.pi * t / c)
    return y, t

# Defines a wrapper function
def dist_foobar(x, num_iter=200, **kwargs):  # pylint: disable=invalid-name
    """Wrapper function that gets a distribution of results from foobar """

    _, t = foobar(x, **kwargs)
    output_shape = (t.size, num_iter)
    y_dist = np.zeros(output_shape)

    for i in range(num_iter):
        y_dist[:, i], _ = foobar(x, **kwargs)

    y_mean = np.mean(y_dist, axis=1)
    y_stdev = np.std(y_dist, axis=1)

    return y_mean, y_stdev, t


# Defines a function for plotting x
def plot_x(site):
    """ Plots the graph with fixed x """

    y_mean, y_stdev, t = site.function(site.x, **site.function_kwargs) 
    data = pd.DataFrame({
        "Time (s)" : t,
        "Predicted" : y_mean,
        "+Stdev" : y_mean + y_stdev,
        "-Stdev" : y_mean - y_stdev,
    })
    fig = data.plot(
        x="Time (s)", y=["Predicted", "-Stdev", "+Stdev"], title="Fixed x",
    )
    return fig


# Lists the inputs for the function
input_dict_list = [
    {
        "name" : "Number of Samples",   # Name to be displayed on the web app.
        "id" : "num_iter",              # Internal id and keyword argument for the function (dist_foobar).
        "value" : 1000,                 # Initial value.
        "type" : "number",              # Used by Dash to determine the input type.
    },
    {
        "name" : "Amplitude Mean",
        "id" : "a_mean",
        "value" : 1,
        "type" : "number",
    },
    {
        "name" : "Amplitude Stdev",
        "id" : "a_stdev",
        "value" : .1,
        "type" : "number",
    },
    {
        "name" : "x Period Mean",
        "id" : "b_mean",
        "value" : 2,
        "type" : "number",
    },
    {
        "name" : "x Period Stdev",
        "id" : "b_stdev",
        "value" : .2,
        "type" : "number",
    },
    {
        "name" : "t Period Mean",
        "id" : "c_mean",
        "value" : 3,
        "type" : "number",
    },
    {
        "name" : "t Period Stdev",
        "id" : "c_stdev",
        "value" : .3,
        "type" : "number",
    },
    {
        "name" : "Maximum t",
        "id" : "tmax",
        "value": 1,
        "type" : "number",
    },
]

boundaries = {     # Locations can be selected from a drop-down menu
        "Vessel 1" : [0, np.pi],          # [Start, End]
        "Vessel 2" : [np.pi, 3 * np.pi],  
        "Vessel 3" : [3 * np.pi, 7 * np.pi],
}

site = visnet1d.Site(
        dist_foobar,        # Function to plot.
        boundaries,         # Vessel locations to select.
        input_dict_list,    # List of function inputs (each input has it's own dictionary).
        plot=plot_x,        # Function for plotting with static x.
)

# Runs the server with the default arguments - same as site.run()
site.run(host="127.0.0.1", port="8050", debug=False)
