#! /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
import pandas as pd

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
import visnet1d

# Let's explore a more typical real world example using the example csv files.
# First, let's get our output function
out_mat = pd.read_csv('examples/network_pressure.csv', header=None).to_numpy()
t = pd.read_csv('examples/network_time.csv', header=None).to_numpy().flatten()
output = visnet1d.get_static_function(out_mat, t, axis=0)

# Now we define our network vessel boundaries
bounds = pd.read_csv('examples/network_boundaries.csv', header=None).to_numpy()
names = pd.read_csv('examples/network_names.csv', header=None)[0].tolist()
boundaries = {n : bounds[i, :].astype(np.intc).tolist() for i, n in enumerate(names)}

site = visnet1d.Site(
    output,
    boundaries,
    input_dict_list=None,
    xlab="Time (s)",
    ylab="Pressure kPa",
    title="Arterial Vessel Pressures",
    gwidth=65,                      # Sets the width of the graph to 65% of the screen width.
    image="examples/diagram.png",   # Displays an image under the inputs (useful for visualising the network).
    iheight=80,                     # Sets the height of the image to 80% of the screen height.
)
site.run(host="0.0.0.0", port="8080", debug=False)
