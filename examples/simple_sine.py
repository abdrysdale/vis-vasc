#! /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
import pandas

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
import visnet1d

# Defines a simple function that takes a spatial argument
# along with other keyword arguments.
# Must return two vectors! The x-axis and the y-axis.
def foobar(x, tmax=1, a=2, b=3):
    t = np.linspace(0, tmax, 100)
    y = a * np.sin(x + 2 * np.pi * t / b)
    return y, t

# Defines a list for the function input arguments
input_dict_list = [
    {
        "name" : "Amplitude",   # Name to be displayed on the web app.
        "id" : "a",             # Internal id and keyword argument for the function (foobar).
        "value" : 1,            # Initial value.
        "type" : "number",      # Used by Dash to determine the input type.
    },
    {
        "name" : "Period",
        "id" : "b",
        "value" : 2,
        "type" : "number",
    },
    {
        "name" : "Maximum t",
        "id" : "tmax",
        "value": 1,
        "type" : "number",
    },
]

boundaries = {     # Locations can be selected from a drop-down menu
        "Vessel 1" : [0, np.pi],          # [Start, End]
        "Vessel 2" : [np.pi, 3 * np.pi],  
}

site = visnet1d.Site(
        foobar,             # Function to plot.
        boundaries,         # Locations to select.
        input_dict_list,    # List of function inputs (each input has it's own dictionary).
        xlab="Time (s)",    # X-axis label
        ylab="Response",    # Y-axis label
        title="My title",   # Title
)

# Runs the server with the default arguments - same as site.run()
site.run(host="127.0.0.1", port="8050", debug=False)
